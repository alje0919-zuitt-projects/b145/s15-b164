//ACTIVITY 1 --------------------------
console.log('Hello World');

function enter2Num() {
	let no1 = parseInt(prompt('Input first number:'));
	let no2 = parseInt(prompt('Input second number:'));
	let sum = no1 + no2;
	let diff = no1 - no2;
	let prod = no1 * no2;
	let quo = no1 / no2;

	if (sum < 10) {
		console.warn(`The sum of the two numbers is: ${sum}`);
	}else if(sum > 9 && sum < 21) {
		alert(`The difference of the two numbers is: ${diff}`);
	}else if(sum > 20 && sum < 30) {
		alert(`The product of the two numbers is: ${prod}`);
	}else {
		alert(`The quotient of the two numbers is: ${quo}`);
	}
} 
enter2Num();


//ACTIVITY 2 --------------------------
let userName = prompt('Hi! What is your name?');
let userAge = parseInt(prompt('How old are you?'));

function userInfo() {
	if ((userName === '' || null) || (userAge === '' || null)){
		alert('Are you a time traveller?');
	}else {
		alert(`Hi ${userName}! Congratulations on your ${userAge} years of existence.`);
	}
}
userInfo();


//ACTIVITY 3 --------------------------
function isLegalAge() {
	if(userAge > 17) {
		alert('You are of legal age.');
	} else {
		alert('You are not allowed here.');
	}
}
isLegalAge();


//ACTIVITY 4 --------------------------
/*
function ageChecker() {
	switch(userAge) {
		case ((userAge > 0) && (userAge > 17) && (userAge < 21)):
			alert('You are now allowed to party.');
			break;
		case ((userAge > 20) && (userAge < 65)):
			alert('You are now part of the adult society.');
			break;
		case (userAge > 64):
			alert('We thank you for your contribution to society.');
			break;
		default:
			alert('Are you sure you\'re not an alien?');
	}
}
ageChecker();
*/


function ageChecker() {
	switch(userAge) {
		case 18:
			alert('You are now allowed to party.');
			break;
		case 21:
			alert('You are now part of the adult society.');
			break;
		case 65:
			alert('We thank you for your contribution to society.');
			break;
		default:
			alert('Are you sure you\'re not an alien?');
	}
}
ageChecker();




//ACTIVITY 5 --------------------------

function fetchErr() {
	try {
		alert(isLegalAg());
	}catch (err) {
		console.warn(err.message);
	}finally{
		alert('Refresh browser');
	}
}
fetchErr();