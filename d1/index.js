console.log('Hello World');

//ASSIGNMENT OPERATOR
let assignmentNumber = 8;

//ARITHMETIC OPERATORS
// + - * / %

//ADDITION ASSIGNMENT OPERATOR (+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

//shorthand
assignmentNumber += 2;
console.log(assignmentNumber); //12

//SUBTRACTION/MULTIPLICATION/DIVISION ASSIGNMENT OPERATOR (-=, *=, /=)
assignmentNumber -= 2;
console.log(assignmentNumber);
assignmentNumber *= 2;
console.log(assignmentNumber);
assignmentNumber /= 2;
console.log(assignmentNumber);

//MULTIPLE OPERATORS AND PARENTHESIS
//when multiple operators are applied in a single statement, it follows the PEMDAS rule.

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
// pemdas = 1 + (-1) * 0.8
console.log(pemdas);

//INCREMENT and DECREMENT OPERATOR
//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

//increment
//pre-fix incrementation
++z;
console.log(z); // 2 - the value of z was added with 1 and is immediately returned.

//post-fix incrementation
//the value of "z" is returned and stored in the variable "increment" then the value of 'z' is increased by one
z++;
console.log(z); // 3 - the value of z was added with 1
console.log(z++); //3 - the previous value of the variable is returned.
console.log(z); //4 - a new value is no returned.

//pre-fix vs post-fix incrementation
console.log(z++); //4
console.log(z); //5

console.log(++z); //6
console.log(z); //6


//TYPE COERCION

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion); //string + number = string

let numE = true + 1;
console.log(numE); //2
console.log(typeof numE); //boolean + number = number


//COMPARISON OPERATORS ==
console.log('Comparison Operator');
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true
console.log('juan' == 'JUAN'); //false, case-sensitive
let juan = 'juan';
console.log('juan' == juan); //true


//INEQUALITY OPERATOR !=
//checks whether the operands are not equal/have different content
console.log('Inequality Operator');
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false); //false
console.log('juan' != 'JUAN'); //true
console.log('juan' != juan); //false


//STRICTLY EQUALITY OPERATOR ===
console.log('Strictly Equality Operator');
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //true > false
console.log(0 === false); //true > false
console.log('juan' === 'JUAN'); //false, case-sensitive
console.log('juan' === juan); //true


//STRICTLY INEQUALITY OPERATOR !==
console.log('Strictly Inequality Operator');
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //false > true
console.log(0 !== false); //false > true
console.log('juan' !== 'JUAN'); //true
console.log('juan' !== juan); //false


//RELATIONAL COMPARISON OPERATORS > <
//checks the relationship between the operands
let x = 500;
let y = 700;
let w = 8000;
let numString = "5500";

console.log('Relational Comparison Operators')
//Greater than >
console.log(x > y); //false
console.log(w > y); //true

//Less than <
console.log(w < x); //false
console.log(y < y); //false
console.log(x < 1000); //true
console.log(numString < 1000); //false - forced coercion
console.log(numString < 6000); //true - forced coercion to change string into number
console.log(numString < "Jose"); //true "5500" < "Jose"

//Greater than or Equal to
console.log(w >= 8000); //true

//Lesser than or Equal to
console.log(x <= y); //true
console.log(y <= y); //true


//LOGICAL OPERATORS
let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log("Logical Operators");
console.log("Logical AND Operator");
//Logical AND Operator (&& - Double Ampersand)
//Returns true if all operands are true
let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); //true

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3); //false

let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization4); //true


let userName = "gamer2022";
let userName2 = "shadow1991";
let userAge = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge >= requiredAge;
//.length is a property of strings which determine the number of characters in the string.
console.log(registration1); //false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2); //true


console.log("Logical OR Operator");
//OR Operator (|| Double Pipe)
//returns true if atleast ONE of the operands are true

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement1); //true

let guildRequirement2 = isAdmin || userLevel2 >= requiredLevel;
console.log(guildRequirement2); //false

console.log("Not Operator");
//NOT Operator !
//turns a boolean into the opposite value

let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); //true
console.log(!isRegistered); //false
console.log(!isLegalAge); //false

let opposite = !isAdmin;
let opposite2 = !isLegalAge;
console.log(opposite); //true - isAdmin original value false
console.log(opposite2) //false - isLegalAge original value true

//if, else if, and else statement

//IF statement
//if statement will run a code block if the conditon specified is true or results to true

//if(true){
//	alert("We just run an if condition");
//}

let numG = -1;
if(numG < 0) {
	console.log('Hello');
}

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length > 10){
	console.log("Welcome to Game Online!");
}
if(userLevel3 >= requiredLevel) {
	console.log("You are qualified to join the clan!");
}

if(userName3.length >= 10 && isRegistered && isAdmin){
	console.log("Thank you for joining the Admin!")
} else {
	console.log("You are not ready to be an Admin")
}

//ELSE Statement
//The "else" statement executes a block of codes if all other conditions are false
if(userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge){
	console.log("Thank you for joining the Noobies Guild!");
} else {
	console.log("You are too strong to be a noob. :(")
}


//ELSE-IF Statement
if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
	console.log("Thank you for joining the noobies guild!")
} else if(userLevel > 25) {
	console.log("You are too strong to be a noob.")
} else if(userAge3 < requiredAge) {
	console.log("You are too young to join the guild.")
} else {
	console.log("Better luck next time.")
}


//IF, ELSE IF and ELSE statement with Functions

function addNum(num1, num2) {
	//check if the numbers being passed are number types.
	//typeof keyword returns a string which tells the type of data that follows it
	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both arguments passed are number types");
		console.log(num1 + num2);
	} else {
		console.log("One or both of the arguments are not numbers")
	}
}
addNum(5, '2');


//create log in function

function login(username, password) {
	//check if the argument passed are strings
	if(typeof username === "string" && typeof password === "string"){
		console.log("Both Arguments are strings.")
	
		/*
			nested if-else
				will run if the parent if statement is able to agree to accomplish its condition
			Mini-Activity:

			add another condition to our nested if statement:
                    -check if username is atleast 8 characters long
                    -check if the password is atleast 8 characters long.
                    -show an alert which says "Thank you for logging in"
            add an else statement which will run if both condition were not met:
                    -show an alert which says "Credentials too short."


            Stretch Goals: 

            add an else if statement that if username is less than 8 characters
                    -show an alert "Username is too short"
            add an else if statement if the password is less than 8 characters
                    -show an alert "password too short"

		*/
/*
	if(username.length >= 8 && password.length >= 8){
		alert("Thank you for logging in!");
	} else if(username.length <= 7){
		alert("Username is too short.");
	} else if(password.length <= 7){
		alert("Password is too short.");
	} else{
		alert("Credentials too short");
	}
*/
	}
	else {
		console.log("One of the arguments is not a string")

	}
}
login("asdasdasdas", "asd");


//Function with return keyword

let message = 'No message.';
console.log(message);

function determineTyphoonIntensity(windSpeed) {
	if(windSpeed < 30) {
		return 'Not a typhoon yet.';
	} else if(windSpeed <= 61) {
		return 'Tropical depression detected.';
	} else if(windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical storm detected.';
	} else if(windSpeed >= 89 && windSpeed <= 117) {
		return 'Sever tropical storm detected';
	} else {
		return 'Typhoon detected';
	}
}

message = determineTyphoonIntensity(68);
console.log(message);
//console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.

if (message == 'Tropical storm detected.'){
	console.warn(message);
}

//Truthy and Falsy
//truthy
if(true){
	console.log('Truthy');
} if(1) {
	console.log('True');
}

//falsy
if(false){
	console.log('Falsy');
}

if(undefined) {
	console.log('Falsy');
}

//TERNARY OPERATOR

/*
Syntax;
	(expression/condition) ? ifTrue : ifFalse;
Three operands of ternary operator:
1. condition
2. expression to execute if the condition is truthy
3. expression to execute if the condition is falsy

Ternary operators were created so that we can have an if-else statement in one line
*/

let ternaryResult = (1 < 18) ? true : false;
console.log(`result of ternary operator ${ternaryResult}`);

//let(1<18) {
//	console.log(true);
//} else {
//	console.log(false);
//}

let price = 50000;
price > 1000 ? console.log("price is over 1000") : console.log("price is less than 1000");

//-----------------------------
let villain = "Harvey Dent";

villain === "Two Face"
? console.log("You lived long enough to be a villain.")
: console.log("Not quite villainous yet.");


//Ternary operators have an implicit "return" statement that without return keyword, the resulting expression can be stored in a variable.
//else if ternary operator
let a = 7;

a === 5
? console.log("A")
: (a === 10 ? console.log("A is 10") : console.log("A is not 5 or 10"));


//Multiple statement execution

/*
let name;
function isOfLegalAge() {
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge() {
	name = 'Jane';
	return 'You are under the age limit';
}

let age = parseInt(prompt("What is your Age?"));
console.log(age);

*/

/*
? specifies the 'if'
() specifies the condition
: specifies the 'else'
*/
/*
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log(`Result of Ternary operator in functions: ${legalAge}, ${name}`);
*/


/*-------------------------------------------------------------------------------
Mini Activity

	Monday - Black
	Tuesday - Green
	Wednesday - Yellow
	Thursday - Red
	Friday - Violet
	Saturday - Blue
	Sunday - White

	Today is <dayToday>, wear <colorOfShirt>
	else
	Invalid Input. Enter a valid day of the week.

*/

/* ------my fail answer------

let dayToday = prompt('Today is?');

function colorOfTheDay() {
	if(dayToday = 'Monday' || 'monday' || 'Mon' || 'mon');{
		console.log(`Today is ${dayToday}, wear Black`);
	} else {
		console.log('test');
	}
} 

colorOfTheDay();
*/
// ---------------------------------------------

// INSTRUCTOR'S ANSWER ------------------
function colorOfTheDay(day) {
	//check if the argument passed is a string
	if(typeof day === "string") {

		if(day.toLowerCase() === "monday") {
			alert(`Today is ${day}, wear Black.`)
		} else if(day.toLowerCase() === "tuesday") {
			alert(`Today is ${day}, wear Green.`)
		} else if(day.toLowerCase() === "wednesday") {
			alert(`Today is ${day}, wear Yellow.`)
		} else if(day.toLowerCase() === "thursday") {
			alert(`Today is ${day}, wear Red.`)
		} else if(day.toLowerCase() === "friday") {
			alert(`Today is ${day}, wear Violet.`)
		} else if(day.toLowerCase() === "saturday") {
			alert(`Today is ${day}, wear Blue.`)
		} else if(day.toLowerCase() === "sunday") {
			alert(`Today is ${day}, wear White.`)
		} else {
			alert("Invalid Input.");
		}

	} else {
		alert("Invalid Input. Enter a valid day of the week")
	}
}

let day = prompt("Today is?");
colorOfTheDay(day);


// SWITCH STATEMENT
/*
syntax:
	switch (expression/condition) {
		case value:
			statement;
			break;
		default:
			statement;
			break;
	}
*/

let hero = prompt("Type hero:").toLowerCase();

switch (hero) {
	case "jose rizal":
		console.log("National Hero of the Philippines");
		break;
	case "george washington":
		console.log("Hero of American Revolution");
		break;
	case "hercules":
		console.log("Legendary Hero of Greek");
		break;
	default:
		console.log('Not an option. Please type again');
		break;
}

//-----------another example--------------

function roleChecker(role) {
	switch(role) {
		case "admin":
			console.log("Welcome Admin, to the dashboard.");
			break;
		case "user":
			console.log("You are not authorized to view this page.");
			break;
		case "guest":
			console.log("Go to the registration page to register");
			break;
		default:
			console.log("Invalid role");
			//by default, your switch ends with 'default' case, no break necessary.
	}
}

roleChecker("admin");


// Try-Catch-Finally Statement
//this is used for error handling

function showIntensityAlert(windSpeed) {
	//attempt to execute a code
	try {
		aalert(determineTyphoonIntensity(windSpeed));
	}catch (error) {
		//error / err are commonly used variable by developers for storing errors
		console.log(typeof error);

		//catch error within 'try' statement
		//the "error.message" is used to access the info relating to an error object
		console.warn(error.message);
	}finally{
		//continue execution of code REGARDLESS of success or failure of code execution in the 'try' block to handle/resolve error.
		//optional
		alert('Intensity updates will show new alert')
	}
}

showIntensityAlert(68);

//THROW - user-defined exception
//Execution of the current function will stop

const number = 40;

try {
	if(number > 50) {
		console.log('Success');
	}else {
		//user-defined throw statement
		throw Error('The number is low');
	}
	//if throw executes, the below code does not execute
	console.log('hello');
} catch(error) {
	console.log('An error caught');
	console.warn(error.message);
}

//another example THROW----------------------------------
function getArea(width, height) {
	if(isNaN(width) || isNaN(height)) {
		throw 'Parameter is not a number!'
	}
} try {
	getArea(3, 'A');
} catch(e) {
	console.error(e);
} finally {
	alert('Number only');
}

